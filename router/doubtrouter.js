const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');

const doubt = require('../model/doubt');

const doubtRouter = express.Router();

doubtRouter.use(bodyParser.json());

doubtRouter.route('/')
    .get((req, res, next) => {
        doubt.find({})
            .then((result) => {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(result);
            }, (err) => next(err)).catch((err) => next(err));
    })

    .post(authenticate.verifyUser,  (req, res, next) => {
        doubt.create(req.body).then((result) => {
            res.statusCode = 201;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
            console.log("doubt Created");
        }, (err) => next(err)).catch((err) => next(err));
    })

    .put(authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation is not supported on /course');
    })

    .delete(authenticate.verifyUser,(req, res, next) => {
        doubt.remove({}).then((result) => {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
        }, (err) => next(err)).catch((err) => next(err));
    })

doubtRouter.route('/:doubtId')
    .get((req, res, next) => {
        doubt.findById(req.params.doubtId)
        .then((result) => {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
        }, (err) => next(err)).catch((err) => next(err));
    })

    .post(authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) => {
        res.statusCode = 403;
        res.end('POST operation is not supported on /course/' + req.params.courseId);
    })

    .put(authenticate.verifyUser, (req, res, next) => {
        doubt.findByIdAndUpdate(req.params.doubtId, {
            $set: req.body
        }, {
            new: true
        }).then((result) => {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
        }, (err) => next(err)).catch((err) => next(err));
    })

    .delete(authenticate.verifyUser, (req, res, next) => {
        doubt.findByIdAndRemove(req.params.doubtId).then((result) => {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
        }, (err) => next(err)).catch((err) => next(err));
    });

doubtRouter.route('/:doubtId/comments')
    .get((req, res, next) => {
        doubt.findById(req.params.doubtId).then((doubt) => {
            if (doubt != null) {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(doubt.comments);
            } else {
                err = new Error('course ' + req.params.courseId + ' not found');
                err.status = 404;
                return next(err);
            }
        }, (err) => next(err)).catch((err) => next(err));
    })

    .post(authenticate.verifyUser,(req, res, next) => {
        doubt.findById(req.params.doubtId).then((doubt) => {
            if (doubt != null) {
              //  req.body.author = req.user._id;
               doubt.comments.push(req.body);
                doubt.save().then((doubt) => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(doubt);
                }, (err) => next(err)).catch((err) => next(err));
            } else {
                err = new Error('doubt ' + req.params.doubtId + ' not found');
                err.status = 404;
                return next(err);
            }
        }, (err) => next(err)).catch((err) => next(err));
    })

    .put(authenticate.verifyUser,(req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation is not supported on /doubt/' + req.params.doubtId + '/comments');
    })

    .delete(authenticate.verifyUser,(req, res, next) => {
        doubt.findById(req.params.doubtId).then((doubt) => {
            if (doubt != null) {
                console.log(doubt);
                for (var i = (doubt.comments.length - 1); i >= 0; i--) {
                    doubt.comments.id(doubt.comments[i]._id).remove();
                }
                doubt.save().then((doubt) => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(doubt);
                }, (err) => next(err)).catch((err) => next(err));
            } else {
                err = new Error('doubt ' + req.params.doubtId + ' not found');
                err.status = 404;
                return next(err);
            }
        }, (err) => next(err)).catch((err) => next(err));
    })

doubtRouter.route('/:doubtId/comments/:commentId')
    .get((req, res, next) => {
        doubt.findById(req.params.doubtId).then((doubt) => {
            if (doubt != null && doubt.comments.id(req.params.commentId)) {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(doubt.comments.id(req.params.commentId));
            } else if (doubt == null) {
                err = new Error('doubt ' + req.params.doubtId + ' not found');
                err.status = 404;
                return next(err);
            } else {
                err = new Error('Comment ' + req.params.commentId + ' not found');
                err.status = 404;
                return next(err);
            }
        }, (err) => next(err)).catch((err) => next(err));
    })

    .post(authenticate.verifyUser,(req, res, next) => {
        res.statusCode = 403;
        res.end('POST operation is not supported on /doubt/' + req.params.doubtId + '/comments/' + req.params.commentId);
    })

    .put( (req, res, next) => {
        doubt.findById(req.params.doubtId).then((doubt) => {
            if (doubt != null && doubt.comments.id(req.params.commentId)) {
                if (doubt.comments.id(req.params.commentId).author.toString() != req.user._id.toString()) {
                    err = new Error('You are not authorized to edit this comment');
                    err.status = 403;
                    return next(err);
                }


                if (req.body.comment) {
                    doubt.comments.id(req.params.commentId).comment = req.body.comment;
                }
                doubt.save().then((doubt) => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(doubt);
                }, (err) => next(err)).catch((err) => next(err));
            } else if (doubt == null) {
                err = new Error('doubt ' + req.params.doubtId + ' not found');
                err.status = 404;
                return next(err);
            } else {
                err = new Error('Comment ' + req.params.commentId + ' not found');
                err.status = 404;
                return next(err);
            }
        }, (err) => next(err)).catch((err) => next(err));
    })

    .delete(authenticate.verifyUser,(req, res, next) => {
        doubt.findById(req.params.doubtId).then((doubt) => {
            if (doubt != null && doubt.comments.id(req.params.commentId)) {
                if (doubt.comments.id(req.params.commentId).author.toString() != req.user._id.toString()) {
                    err = new Error('You are not authorized to edit this comment');
                    err.status = 403;
                    return next(err);
                }
                doubt.comments.id(req.params.commentId).remove();
                doubt.save().then((doubt) => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(doubt);
                }, (err) => next(err)).catch((err) => next(err));
            } else if (doubt == null) {
                err = new Error('doubt ' + req.params.doubtId + ' not found');
                err.status = 404;
                return next(err);
            } else {
                err = new Error('Comment ' + req.params.commentId + ' not found');
                err.status = 404;
                return next(err);
            }
        }, (err) => next(err)).catch((err) => next(err));
    });

module.exports = doubtRouter;
