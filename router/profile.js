const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var authenticate = require('../authenticate');
const profile = require('../model/faculty');
var nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
var bcrypt = require('bcrypt-nodejs');
var async = require('async');
var crypto = require('crypto');
const profileRouter = express.Router();
profileRouter.use(bodyParser.json());
profileRouter.route('/')
.get((req, res, next) => {
    profile.find({})
        .then((result) => {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
        }, (err) => next(err)).catch((err) => next(err));
});

profileRouter.route('/:profileId')
.get((req, res, next) => {
       profile.findById(req.params.profileId,{'femail':1,'_id':0,'username':1},(err,profile)=> {
           res.statusCode = 200;
           res.setHeader("Content-Type", "application/json");
           res.json(profile);
       }, (err) => next(err));
   })
   .put(authenticate.verifyUser,(req, res, next) => {
     var username = req.body.username;
var femail = req.body.femail;


        profile.findOneAndUpdate(req.params.profileId, {
            $set:{username:username,femail:femail}
        }, {
            new: true
        }).then((dish) => {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
          res.json({username:username,femail:femail});
           //res.redirect('/profile?Id='+dish_id);
        }, (err) => next(err)).catch((err) => next(err));
    })

    profileRouter.post( '/update/reset', function(req, res, next) {
      async.waterfall([
        function(done) {
          crypto.randomBytes(20, function(err, buf) {
            console.log("hello token");
            var token = buf.toString('hex');
            done(err, token);
            console.log("hello done");
          });
        },
        function(token, done) {
        //  Userss=new faculty({femail:req.body.femail});
          profile.findOne({femail: req.body.femail})
            .then(user => {
                if (!user) return res.status(401).json({message: 'The email address ' + req.body.femail + ' is not associated with any account. Double-check your email address and try again.'});



            user.resetPasswordToken = token;
            user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
    console.log("email exists");
    //console.log(email);
    console.log(token);
            user.save(function(err) {
              done(err, token, user);
            });
          });
        },

        function(token, user, done) {
          var Transport = nodemailer.createTransport({
            host:'smtp.gmail.com',
            port: 587,
            sercure:false,
            requireTLS:true,


              auth: {
                //  console.log("hello mail");
                user: "npatni846@gmail.com",    // SendGrid Username
                pass: "sfrgboxydrrulhpq"    // SendGrid Password
              }
            });
          console.log("hiiiiiiiiiiiiiiii");

          // Email Details
          var mailOptions = {
            to: user.femail,
            from: 'passwordreset@demo.com',
            subject: 'Express.js Password Reset',
            text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
              'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
              'http://' + req.headers.host + '/reset/' + token + '\n\n' +
              'If you did not request this, please ignore this email and your password will remain unchanged.\n'
          };
          console.log(user.femail);
    console.log("mail sent");

          Transport.sendMail(mailOptions, (err,done) => {
            if (err) {
              return next(err);
              console.log("mail not send");
              // handle error
            } else {
               console.log("yupppppppppppppp");
              // handle success
              console.log('info', 'An e-mail has been sent to ' + user.femail + ' with further instructions.');
    return res.status(201).json({message:  'An e-mail has been sent to ' + user.femail + ' with further instructions.'});

            //  req.flash('info', 'An e-mail has been sent to ' + user.femail + ' with further instructions.');
              done(err, 'done');
              console.log('hqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq');
              return res.status(201).json({message:  'An e-mail has been sent to ' + user.femail + ' with further instructions.'});
            }
          });
        }
      ], function(err) {
        if (err) return next(err);
        res.redirect('/forgot');
      });
    });

    profileRouter.route('/:token')

    .post(authenticate.verifyUser, function(req, res) {
      async.waterfall([
        function(done) {
          profile.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
            if (!user) {
              console.log("heeeeeeeeeeeeeeeeeeeeeeeeeeee");

        //      req.flash('error', 'Password reset token is invalid or has expired.');
              return res.redirect('/');
            }

            user.password = req.body.password;
            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;

            user.save(function(err) {
              req.logIn(user, function(err) {
                console.log(user.password);
                done(err, user);
              });
            });
          });
        },
        function(user, done) {
          var Transport = nodemailer.createTransport({
            host:'smtp.gmail.com',
            port: 587,
            sercure:false,
            requireTLS:true,
            auth: {
              //  console.log("hello mail");
              user: "npatni846@gmail.com",    // SendGrid Username
              pass: "sfrgboxydrrulhpq"    // SendGrid Password
            }


          }


        );
        console.log("hiiiiiiiiiiiiiiii");


          // Email Details
          var mailOptions = {
            to: user.femail,
            from: 'passwordreset@demo.com',
            subject: 'Your password has been changed',
            text: 'Hello,\n\n' +
              'This is a confirmation that the password for your account ' + user.femail + ' has just been changed.\n'
          };

          // Send Email
          Transport.sendMail(mailOptions, (err, resp) => {
            if (err) {
              // handle error
            } else {
              return res.status(201).json({message:  'An password has been reset'});

              // handle success
            //  req.flash('success', 'Success! Your password has been changed.');
              done(err);
            }
          });
        }
      ], function(err) {
        res.redirect('/');
      });
    });




module.exports = profileRouter;
