const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');

const course = require('../model/course');

const courseRouter = express.Router();

courseRouter.use(bodyParser.json());

courseRouter.route('/')
    .get((req, res, next) => {
        course.find({})
            .then((result) => {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(result);
            }, (err) => next(err)).catch((err) => next(err));
    })

    .post(authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
        course.create(req.body).then((result) => {
            res.statusCode = 201;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
            console.log("course Created");
        }, (err) => next(err)).catch((err) => next(err));
    })

    .put(authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation is not supported on /course');
    })

    .delete(authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) => {
        course.remove({}).then((result) => {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
        }, (err) => next(err)).catch((err) => next(err));
    })

courseRouter.route('/:courseId')
    .get((req, res, next) => {
        course.findById(req.params.courseId)
        .then((result) => {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
        }, (err) => next(err)).catch((err) => next(err));
    })

    .post(authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) => {
        res.statusCode = 403;
        res.end('POST operation is not supported on /course/' + req.params.courseId);
    })

    .put(authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) => {
        course.findByIdAndUpdate(req.params.courseId, {
            $set: req.body
        }, {
            new: true
        }).then((result) => {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
        }, (err) => next(err)).catch((err) => next(err));
    })

    .delete(authenticate.verifyUser, authenticate.verifyAdmin,(req, res, next) => {
        course.findByIdAndRemove(req.params.courseId).then((result) => {
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(result);
        }, (err) => next(err)).catch((err) => next(err));
    });

courseRouter.route('/:courseId/comments')
    .get((req, res, next) => {
        course.findById(req.params.courseId).then((result) => {
            if (course != null) {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(course.comments);
            } else {
                err = new Error('course ' + req.params.courseId + ' not found');
                err.status = 404;
                return next(err);
            }
        }, (err) => next(err)).catch((err) => next(err));
    })

    .post(authenticate.verifyUser,(req, res, next) => {
        course.findById(req.params.courseId).then((course) => {
            if (course != null) {
              //  req.body.author = req.user._id;
               course.comments.push(req.body);
                course.save().then((result) => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(course);
                }, (err) => next(err)).catch((err) => next(err));
            } else {
                err = new Error('course ' + req.params.courseId + ' not found');
                err.status = 404;
                return next(err);
            }
        }, (err) => next(err)).catch((err) => next(err));
    })

    .put(authenticate.verifyUser,(req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation is not supported on /dishes/' + req.params.courseId + '/comments');
    })

    .delete(authenticate.verifyUser,(req, res, next) => {
        course.findById(req.params.courseId).then((course) => {
            if (course != null) {
                console.log(course);
                for (var i = (course.comments.length - 1); i >= 0; i--) {
                    course.comments.id(course.comments[i]._id).remove();
                }
                course.save().then((course) => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(course);
                }, (err) => next(err)).catch((err) => next(err));
            } else {
                err = new Error('course ' + req.params.courseId + ' not found');
                err.status = 404;
                return next(err);
            }
        }, (err) => next(err)).catch((err) => next(err));
    })

courseRouter.route('/:courseId/comments/:commentId')
    .get((req, res, next) => {
        course.findById(req.params.courseId).then((course) => {
            if (course != null && course.comments.id(req.params.commentId)) {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(course.comments.id(req.params.commentId));
            } else if (course == null) {
                err = new Error('Course ' + req.params.courseId + ' not found');
                err.status = 404;
                return next(err);
            } else {
                err = new Error('Comment ' + req.params.commentId + ' not found');
                err.status = 404;
                return next(err);
            }
        }, (err) => next(err)).catch((err) => next(err));
    })

    .post(authenticate.verifyUser,(req, res, next) => {
        res.statusCode = 403;
        res.end('POST operation is not supported on /course/' + req.params.courseId + '/comments/' + req.params.commentId);
    })

    .put(authenticate.verifyUser, (req, res, next) => {
        course.findById(req.params.courseId).then((course) => {
            if (course != null && course.comments.id(req.params.commentId)) {
                if (course.comments.id(req.params.commentId).author.toString() != req.user._id.toString()) {
                    err = new Error('You are not authorized to edit this comment');
                    err.status = 403;
                    return next(err);
                }
                if (req.body.rating) {
                    course.comments.id(req.params.commentId).rating = req.body.rating;
                }

                if (req.body.comment) {
                    dish.comments.id(req.params.commentId).comment = req.body.comment;
                }
                course.save().then((course) => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(course);
                }, (err) => next(err)).catch((err) => next(err));
            } else if (course == null) {
                err = new Error('course ' + req.params.courseId + ' not found');
                err.status = 404;
                return next(err);
            } else {
                err = new Error('Comment ' + req.params.commentId + ' not found');
                err.status = 404;
                return next(err);
            }
        }, (err) => next(err)).catch((err) => next(err));
    })

    .delete(authenticate.verifyUser,(req, res, next) => {
        course.findById(req.params.courseId).then((course) => {
            if (course != null && course.comments.id(req.params.commentId)) {
                if (course.comments.id(req.params.commentId).author.toString() != req.user._id.toString()) {
                    err = new Error('You are not authorized to edit this comment');
                    err.status = 403;
                    return next(err);
                }
                course.comments.id(req.params.commentId).remove();
                course.save().then((course) => {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "application/json");
                    res.json(course);
                }, (err) => next(err)).catch((err) => next(err));
            } else if (course == null) {
                err = new Error('course ' + req.params.courseId + ' not found');
                err.status = 404;
                return next(err);
            } else {
                err = new Error('Comment ' + req.params.commentId + ' not found');
                err.status = 404;
                return next(err);
            }
        }, (err) => next(err)).catch((err) => next(err));
    });

module.exports = courseRouter;
