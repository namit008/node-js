const express = require('express');
const app=express();
const bodyParser = require('body-parser');
var authenticate = require('../authenticate');
const faculty = require('../model/faculty');
const facultyRouter = express.Router();
facultyRouter.use(bodyParser.json());
var passport = require('passport');
const accountSid = 'ACe3c34c25f92a5b465708234a16ef43c7';
const authToken = '925c2061c617de76b9cbf9b5e4d3a0e9';
const client = require('twilio')(accountSid,authToken);
var LocalStrategy = require('passport-local').Strategy;
var nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
var bcrypt = require('bcrypt-nodejs');
var async = require('async');
var crypto = require('crypto');
var flash = require('express-flash');
app.use(flash());


facultyRouter.post('/signup', (req, res, next) => {

  Users=new faculty({femail : req.body.femail,mobile_no : req.body.mobile_no, mobile_type : req.body.mobile_type, username : req.body.username});
  faculty.register(Users, req.body.password, function(err, user) {


    if(err) {
      res.statusCode = 500;
      res.setHeader('Content-Type', 'application/json');
      res.json({err: err});
    }
    else {
      passport.authenticate('local')(req, res, () => {
        var token = authenticate.getToken({_id: req.user._id});

        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json({success: true,token:token, status: 'Registration Successful!'});
      });
    }
  });
});
facultyRouter.post('/varifynumber', function(req, res, next) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(4, function(err, buf) {
        console.log("hello token");
        var token = buf.toString('hex');
        done(err, token);
        console.log("hello done");
      });
    },
    function(token, done) {

      faculty.findOne({mobile_no: req.body.mobile_no})
        .then(user => {
            if (!user) return res.status(401).json({message: 'The mobile_no' + req.body.mobile_no + ' is not associated with any account. Double-check your mobile_no and try again.'});
        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour


console.log(token);
        user.save(function(err) {
          done(err, token, user);
        });
      });
    },
function(token, user, done) {

        client.messages
        .create({
           body: 'this otp is for varification\n'+token+'\tdont share with any body',
           from: '+14703090061',
           to: user.mobile_no
         })
        .then(message => console.log(message.sid));

        console.log('info', 'An message has been sent to ' + user.mobile_no + ' with further instructions.');
return res.status(201).json({message:  'An message has been sent to ' + user.mobile_no + ' with further instructions.'});

        //  req.flash('info', 'An e-mail has been sent to ' + user.femail + ' with further instructions.');
          done(err, 'done');
        }


  ], function(err) {
    if (err) return next(err);
    res.redirect('/');
  });
});





facultyRouter.post('/validate', function(req, res) {

faculty.findOne({ resetPasswordToken: req.body.token }, function (err, token) {
       if (!token) return res.status(400).send({ type: 'not-verified', msg: 'We were unable to find a valid token. Your token my have expired.' });

       // If we found a token, find a matching user
       faculty.findOne({mobile_no: req.body.mobile_no }, function (err, user) {
           if (!user) return res.status(400).send({ msg: 'We were unable to find a user for this token.' });
           if (user.isVerified) return res.status(400).send({ type: 'already-verified', msg: 'This user has already been verified.' });

           // Verify and save the user
           user.isVerified = true;
           user.resetPasswordToken = undefined;
           user.resetPasswordExpires = undefined;

           user.save(function (err) {
               if (err) { return res.status(500).send({ msg: err.message }); }
               res.status(200).send("The account has been verified. Please log in.");
           });
       });
     });
   });

passport.use(new LocalStrategy({usernameField:'femail'},function(femail, password, done) {
    faculty.findOne({
        femail: femail

    }, function(err, user) {
      console.log(femail);

        // This is how you handle error
        if (err) {
console.log('errrrrrrrrrrrrrrr');
          return done(err);}
        // When user is not found
        if (!user) {
console.log('erororooroor');
          return done(null, false);}
        // When password is not correct
        if (!user.authenticate(password)) {
console.log('passssssssssss');
          return done(null, false);}
return done(null, user);

     });
}
));

facultyRouter.post('/login', passport.authenticate('local'), (req, res) => {
  var token = authenticate.getToken({_id: req.user._id});
  res.statusCode = 200;
  res.setHeader('Content-Type', 'application/json');
  res.json({success: true,token:token, status: 'You are successfully logged in!'});
});

facultyRouter.get("/logout",(req,res)=>{
    req.logout();
  });



facultyRouter.route('/')
.get((req,res,next) => {
    faculty.find({})
    .then((std) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(std);


    }, (err) => next(err))
    .catch((err) => next(err));
})
/*.post((req, res, next) => {
    faculty.create(req.body)
    .then((std) => {
        console.log('student Created ', std);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(std);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.put((req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /student');
})*/
.delete((req, res, next) => {
    faculty.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

facultyRouter.get('/forgot', function(req, res) {
res.render('forgot', {
   user: req.user
  });
});


facultyRouter.post('/forgot', function(req, res, next) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(20, function(err, buf) {
        console.log("hello token");
        var token = buf.toString('hex');
        done(err, token);
        console.log("hello done");
      });
    },
    function(token, done) {

      faculty.findOne({femail: req.body.femail})
        .then(user => {
            if (!user) return res.status(401).json({message: 'The email address ' + req.body.femail + ' is not associated with any account. Double-check your email address and try again.'});
        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
console.log("email exists");

console.log(token);
        user.save(function(err) {
          done(err, token, user);
        });
      });
    },
function(token, user, done) {
      var Transport = nodemailer.createTransport({
        host:'smtp.gmail.com',
        port: 587,
        sercure:false,
        requireTLS:true,


          auth: {

            user: "npatni846@gmail.com",    // SendGrid Username
            pass: "sfrgboxydrrulhpq"    // SendGrid Password
          }
  });
      console.log("hiiiiiiiiiiiiiiii");

      // Email Details
      var mailOptions = {
        to: user.femail,
        from: 'passwordreset@demo.com',
        subject: 'Express.js Password Reset',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      };
      console.log(user.femail);
console.log("mail sent");

      Transport.sendMail(mailOptions, (err,done) => {
        if (err) {
          return next(err);
          console.log("mail not send");
          // handle error
        } else {
           console.log("yupppppppppppppp");
          // handle success
          console.log('info', 'An e-mail has been sent to ' + user.femail + ' with further instructions.');
return res.status(201).json({message:  'An e-mail has been sent to ' + user.femail + ' with further instructions.'});

        //  req.flash('info', 'An e-mail has been sent to ' + user.femail + ' with further instructions.');
          done(err, 'done');
          console.log('hqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq');
          return res.status(201).json({message:  'An e-mail has been sent to ' + user.femail + ' with further instructions.'});
        }
      });
    }
  ], function(err) {
    if (err) return next(err);
    res.redirect('/forgot');
  });
});

facultyRouter.get('/reset/:token', function(req, res) {
  faculty.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {

//  faculty.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
    //  req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('/forgot');
    }
    res.render('reset', {
      user: req.user
    });
  });
});

facultyRouter.post('/reset/:token', function(req, res) {
  async.waterfall([
    function(done) {
      faculty.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
        if (!user) {


    //      req.flash('error', 'Password reset token is invalid or has expired.');
          return res.redirect('/');
        }

        user.password = req.body.password;
        user.resetPasswordToken = undefined;
        user.resetPasswordExpires = undefined;

        user.save(function(err) {
          req.logIn(user, function(err) {
            console.log(user.password);
            done(err, user);
          });
        });
      });
    },
    function(user, done) {
      var Transport = nodemailer.createTransport({
        host:'smtp.gmail.com',
        port: 587,
        sercure:false,
        requireTLS:true,
        auth: {
          //  console.log("hello mail");
          user: "npatni846@gmail.com",    // SendGrid Username
          pass: "sfrgboxydrrulhpq"    // SendGrid Password
        }
      });



      // Email Details
      var mailOptions = {
        to: user.femail,
        from: 'passwordreset@demo.com',
        subject: 'Your password has been changed',
        text: 'Hello,\n\n' +
          'This is a confirmation that the password for your account ' + user.femail + ' has just been changed.\n'
      };

      // Send Email
      Transport.sendMail(mailOptions, (err, resp) => {
        if (err) {
          // handle error
        } else {
          return res.status(201).json({message:  'An password has been reset'});

          // handle success
        //  req.flash('success', 'Success! Your password has been changed.');
          done(err);
        }
      });
    }
  ], function(err) {
    res.redirect('/');
  });
});
facultyRouter.post('/forgetpassword', function(req, res, next) {
  async.waterfall([
    function(done) {
      crypto.randomBytes(4, function(err, buf) {
        console.log("hello token");
        var token = buf.toString('hex');
        done(err, token);
        console.log("hello done");
      });
    },
    function(token, done) {

      faculty.findOne({mobile_no: req.body.mobile_no})
        .then(user => {
            if (!user) return res.status(401).json({message: 'The mobile_no' + req.body.mobile_no + ' is not associated with any account. Double-check your mobile_no and try again.'});
        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour


console.log(token);
        user.save(function(err) {
          done(err, token, user);
        });
      });
    },
function(token, user, done) {

        client.messages
        .create({
           body: 'this otp is for reset\n'+token+'\tdont share with any body',
           from: '+14703090061',
           to: user.mobile_no
         })
        .then(message => console.log(message.sid));

        console.log('info', 'An message has been sent to ' + user.mobile_no + ' with further instructions.');
return res.status(201).json({message:  'An message has been sent to ' + user.mobile_no + ' with further instructions.'});

        //  req.flash('info', 'An e-mail has been sent to ' + user.femail + ' with further instructions.');
          done(err, 'done');
        }


  ], function(err) {
    if (err) return next(err);
    res.redirect('/forgot');
  });
});

facultyRouter.get('/resetpassword/:token', function(req, res) {
  faculty.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {

//  faculty.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
    //  req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('/forgot');
    }
    res.render('reset', {
      user: req.user
    });
  });
});

facultyRouter.post('/resetpassword/:token', function(req, res) {
  async.waterfall([
    function(done) {
      faculty.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
        if (!user) {
  //      req.flash('error', 'Password reset token is invalid or has expired.');
          return res.redirect('/');
        }

        user.password = req.body.password;
        user.resetPasswordToken = undefined;
        user.resetPasswordExpires = undefined;

        user.save(function(err) {
          req.logIn(user, function(err) {
            console.log(user.password);
            done(err, user);
          });
        });
      });
    },
    function(user, done) {

      client.messages
      .create({
         body: 'Your password has been changed successfully',
         from: '+14703090061',
         to: user.mobile_no
       })
      .then(message => console.log(message.sid));

      console.log('Password changed');
    //  return res.status(201).json({message:  'An e-mail has been sent to ' + user.mobile_no + ' with further instructions.'});
    return res.status(201).json({message:  'An password has been reset'});

      //  req.flash('info', 'An e-mail has been sent to ' + user.femail + ' with further instructions.');
        done(err, 'done');
        return res.status(201).json({message:  'An password has been reset'});

          // handle success
        //  req.flash('success', 'Success! Your password has been changed.');
          done(err);
        }
  ], function(err) {
    res.redirect('/');
  });
});
module.exports = facultyRouter;
