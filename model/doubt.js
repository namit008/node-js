const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const commentSchema = new Schema({

    comment:  {
        type: String,
        required: true
    }
}, {
    timestamps: true
});
const doubtSchema=new Schema({
  subject: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true
    },
    image: {
        type: String,
      //  required: true
    },
    title: {
        type: String,
        required: true
    },


    comments:[commentSchema]
}, {
    timestamps: true
});


var doubt =mongoose.model('doubt',doubtSchema);

module.exports=doubt;
