const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const validator = require('validator');

var passport = require('passport');
var passportLocalMongoose = require('passport-local-mongoose');
const facultyschema = new Schema({

  image:{
    type:String
  },
  mobile_no:{
    type:String
  },
  mobile_type:{
    type:String
  },

   femail:{
       type:String,
       default:" ",
       unique:true,
       required:true
    },

    admin:   {
        type: Boolean,
        default: false
    },
isVerified:{
  type:Boolean,
  default:false
},


    resetPasswordToken: {
        type: String,
        required: false
    },

    resetPasswordExpires: {
        type: Date,
        required: false
    }
},



{
    timestamps: true
});
facultyschema.plugin(passportLocalMongoose);


module.exports =  mongoose.model('faculty',facultyschema);
