const express=require('express');
const app=express();
const mongoose = require('mongoose');
var passport = require('passport');
var authenticate = require('./authenticate');

const faculty = require('./model/faculty');
const facultyRouter = require('./router/facultyroute');
var config = require('./config');
const url = config.mongoUrl;

const profileRouter = require('./router/profile');
const courseRouter =require('./router/courserouter')
const doubtRouter =require('./router/doubtrouter')

const connect = mongoose.connect(url);


connect.then((db) => {

    console.log('Connected correctly to server');

    app.use(passport.initialize());
    
    app.use('/faculty', facultyRouter);
    app.use('/notice', noticeRouter);
    
    app.use('/profile',profileRouter);
app.use('/course',courseRouter);
app.use('/doubt',doubtRouter);

});
app.listen(3000, () => {
    console.log(' Server is running ');
  });
  function auth (req, res, next) {
    console.log(req.user);

    if (!req.user) {
      var err = new Error('You are not authenticated!');
      err.status = 403;
      next(err);
    }
    else {
          next();
    }
}
